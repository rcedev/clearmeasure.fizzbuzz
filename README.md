This repository contains the .NET libraries for Clear Measure customers to generate "FizzBuzz" data.

The src folder contains all source code for the repository.

The build folder contains all components required to compile the source code. Simply execute the build.bat file to compile the solution and run all unit tests. This batch file can be used in a Continuous Integration pipeline as well.

The ClearMeasure.FizzBuzz.Core.IFizzBuzzProvider interface is used to generate FizzBuzz results. Currently, the only implementation of this interface exists in the ClearMeasure.FizzBuzz.Providers.ModulusProvider class (see class for documentation details). The
IFizzBuzzProvider interface can be implemented N number of times to support varying FizzBuzz calculation requirements.

The "Client" console application in this solution contains one example of how a customer can use the ModulusProvider to
generate FizzBuzz results. The ModulusProvider GetResults method will accept a Positive integer of any size, all the way up 
to int.MaxValue.