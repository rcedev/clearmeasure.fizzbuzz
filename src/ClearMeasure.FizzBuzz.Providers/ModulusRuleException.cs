﻿using System;
using ClearMeasure.FizzBuzz.Core;

namespace ClearMeasure.FizzBuzz.Providers
{
    /// <summary>
    ///     An exception class for errors raised in a ModulusRule object.
    /// </summary>
    public class ModulusRuleException : FizzBuzzException
    {
        /// <summary>
        ///     Create a ModulusRuleException.
        /// </summary>
        /// <param name="detailMessage">
        ///     A detail message describing the reason the exception was thrown.
        /// </param>
        public ModulusRuleException(string detailMessage) : base(detailMessage)
        {
        }

        /// <summary>
        ///     Create a ModulusRuleException.
        /// </summary>
        /// <param name="innerException">
        ///     An exception that is the cause of this exception being thrown.
        /// </param>
        public ModulusRuleException(Exception innerException) : base(innerException)
        {
        }

        /// <summary>
        ///     Create a ModulusRuleException.
        /// </summary>
        /// <param name="detailMessage">
        ///     A detail message describing the reason the exception was thrown.
        /// </param>
        /// <param name="innerException">
        ///     An exception that is the cause of this exception being thrown.
        /// </param>
        public ModulusRuleException(string detailMessage, Exception innerException) : base(detailMessage,
            innerException)
        {
        }
    }
}