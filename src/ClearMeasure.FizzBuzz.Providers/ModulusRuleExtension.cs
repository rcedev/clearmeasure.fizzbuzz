﻿using System.Collections.Generic;
using System.Linq;
using ClearMeasure.FizzBuzz.Core;

namespace ClearMeasure.FizzBuzz.Providers
{
    /// <summary>
    ///     Extension methods for Modulus Rules.
    /// </summary>
    internal static class ModulusRuleExtension
    {
        /// <summary>
        ///     Validate a ModulusRule collection.
        /// </summary>
        /// <param name="rules">
        ///     The ModulusRule collection.
        /// </param>
        internal static void Validate(this IEnumerable<ModulusRule> rules)
        {
            if (rules == null) throw new FizzBuzzException("The 'rules' IEnumerable parameter cannot be null.");
            if (!rules.Any()) throw new FizzBuzzException("The 'rules' IEnumerable parameter cannot be empty.");
        }
    }
}