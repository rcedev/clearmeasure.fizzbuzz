﻿using System.Linq;

namespace ClearMeasure.FizzBuzz.Providers
{
    /// <summary>
    ///     A modulus rule.
    /// </summary>
    public sealed class ModulusRule
    {
        /// <summary>
        ///     Create a new ModulusRule.
        /// </summary>
        /// <param name="multiples">
        ///     An array of integers.
        /// </param>
        /// <param name="output"></param>
        public ModulusRule(int[] multiples, string output)
        {
            if (multiples.Length == 0)
                throw new ModulusRuleException("Invalid rule. The rule 'multiples' array cannot be empty.");
            if (multiples.Contains(0))
                throw new ModulusRuleException("Invalid rule. The rule 'multiples' array must be greater than 0.");
            if (string.IsNullOrWhiteSpace(output))
                throw new ModulusRuleException(
                    "Invalid rule. The 'validOutput' property cannot be a null reference or white space value.");

            Multiples = multiples;
            Output = output;
        }

        /// <summary>
        ///     An array of integers. When validating the rule, the integer under inspection must be a multiple
        ///     of all integers in this array for the rule to pass.
        /// </summary>
        public int[] Multiples { get; }

        /// <summary>
        ///     The output string to omit should the rule pass validation.
        /// </summary>
        public string Output { get; }
    }
}