﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClearMeasure.FizzBuzz.Core;

namespace ClearMeasure.FizzBuzz.Providers
{
    /// <summary>
    ///     A class to provide modulus evaluation results.
    /// </summary>
    public class ModulusProvider : IFizzBuzzProvider
    {
        private readonly IEnumerable<ModulusRule> _rules;

        /// <summary>
        ///     Create a new ModulusProvider.
        /// </summary>
        /// <param name="rules">
        ///     The ModulusRules to use when generating output results.
        /// </param>
        public ModulusProvider(IEnumerable<ModulusRule> rules)
        {
            rules.Validate();
            _rules = rules.OrderByDescending(x => x.Multiples.Length);
        }

        /// <summary>
        ///     Get the ModulusProvider results.
        /// </summary>
        /// <param name="iterations">
        ///     The number of iterations to loop through when evaluating against the ModulusRule collection.
        ///     This value should always be greater than 0.
        /// </param>
        /// <returns>
        ///     A collection of strings representing the output of the ModulusProvider calcuation.
        /// </returns>
        /// <remarks>
        ///     The length of this return collection will always match the iterations integer passed in.
        ///     For each iteration, evaluate the iteration int against each ModulusRule, starting with the
        ///     rules with the most ModulusRule.Multiples items.
        ///     Return the ModulusRule.Output string for iterations in which the int is a multiple of
        ///     all of the integers in the ModulusRule.Multiples property.
        ///     Return the iteration integer itself, as a string, if it is not a multiple of any of the
        ///     integers in the ModulusRule.Multiples property.
        /// </remarks>
        public IEnumerable<string> GetResults(int iterations)
        {
            if (iterations <= 0) throw new FizzBuzzException("The 'iterations' parameter must be greater than 0.");
            try
            {
                return YieldOutput(iterations);
            }
            catch (Exception ex)
            {
                throw new FizzBuzzException(ex);
            }
        }

        /// <summary>
        ///     Continuously yield the ModulusProvider results for each iteration.
        /// </summary>
        /// <param name="iterations">
        ///     The number of iterations to evaluate.
        /// </param>
        /// <returns>
        ///     A collection of strings representing the output of the ModulusProvider calcuation.
        /// </returns>
        private IEnumerable<string> YieldOutput(int iterations)
        {
            for (var i = 1; i <= iterations; i++) yield return ProcessRules(i);
        }

        /// <summary>
        ///     Evaluate the input integer against all ModulusRules.
        /// </summary>
        /// <param name="i">
        ///     The input integer to evaluate.
        /// </param>
        /// <returns>
        ///     The ModulusRule.Output property for the first ModulusRule in which
        ///     the input integer is a multiple of all of the integers in the ModulusRule.Multiples property.
        ///     If no matches are found, return the input integer itself, as a string.
        /// </returns>
        private string ProcessRules(int i)
        {
            foreach (var rule in _rules) if (rule.Multiples.All(r => i % r == 0)) return rule.Output;
            return i.ToString();
        }
    }
}