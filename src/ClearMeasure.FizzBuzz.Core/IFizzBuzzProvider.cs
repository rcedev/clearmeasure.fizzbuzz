﻿using System.Collections.Generic;

namespace ClearMeasure.FizzBuzz.Core
{
    /// <summary>
    ///     Abstract interface for generating FizzBuzz results.
    /// </summary>
    public interface IFizzBuzzProvider
    {
        /// <summary>
        ///     Get the IFizzBuzzProvider results.
        /// </summary>
        /// <param name="iterations">
        ///     The number of iterations to loop through to generate results.
        /// </param>
        /// <returns>
        ///     A collection of strings representing the output of the IFizzBuzzProvider calcuation.
        /// </returns>
        IEnumerable<string> GetResults(int iterations);
    }
}