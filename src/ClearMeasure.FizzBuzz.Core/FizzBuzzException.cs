﻿using System;

namespace ClearMeasure.FizzBuzz.Core
{
    /// <summary>
    ///     A core FizzBuzz exception.
    /// </summary>
    public class FizzBuzzException : Exception
    {
        /// <summary>
        ///     Default Message.
        /// </summary>
        private const string DefaultMessage = "A FizzBuzz error has occurred.";

        /// <summary>
        ///     Create a FizzBuzzException.
        /// </summary>
        /// <param name="detailMessage">
        ///     A detail message describing the reason the exception was thrown.
        /// </param>
        public FizzBuzzException(string detailMessage) : base(detailMessage)
        {
        }

        /// <summary>
        ///     Create a FizzBuzzException.
        /// </summary>
        /// <param name="innerException">
        ///     An exception that is the cause of this exception being thrown.
        /// </param>
        public FizzBuzzException(Exception innerException) : base(DefaultMessage, innerException)
        {
        }

        /// <summary>
        ///     Create a FizzBuzzException.
        /// </summary>
        /// <param name="detailMessage">
        ///     A detail message describing the reason the exception was thrown.
        /// </param>
        /// <param name="innerException">
        ///     An exception that is the cause of this exception being thrown.
        /// </param>
        public FizzBuzzException(string detailMessage, Exception innerException) : base(detailMessage, innerException)
        {
        }
    }
}