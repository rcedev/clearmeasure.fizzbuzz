﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ClearMeasure.FizzBuzz.Core;
using ClearMeasure.FizzBuzz.Providers;
using NUnit.Framework;

namespace ClearMeasure.FizzBuzz.Tests
{
    [TestFixture]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ModulusProviderTests
    {
        [Test]
        public void ModulusValueGreaterThanIterationValue_Succeeds()
        {
            var expectedResponse = Guid.NewGuid().ToString();

            var rules = new List<ModulusRule>
            {
                new ModulusRule(new[] {15}, expectedResponse)
            };

            var provider = new ModulusProvider(rules);

            const int iterations = 3;
            var response = provider.GetResults(iterations).ToList();

            for (var i = 1; i <= iterations; i++) Assert.That(response[i - 1] == i.ToString());
        }

        [Test]
        public void MultiModulus_Succeeds()
        {
            var expectedResponse = Guid.NewGuid().ToString();
            var rules = new List<ModulusRule>
            {
                new ModulusRule(new[] {3, 5}, expectedResponse)
            };

            var provider = new ModulusProvider(rules);

            var response = provider.GetResults(15);

            Assert.That(response.Last() == expectedResponse);
        }

        [Test]
        public void SingleModulus_Succeeds()
        {
            var expectedResponse = Guid.NewGuid().ToString();
            var rules = new List<ModulusRule>
            {
                new ModulusRule(new[] {3}, expectedResponse)
            };

            var provider = new ModulusProvider(rules);

            var response = provider.GetResults(3);

            Assert.That(response.Last() == expectedResponse);
        }

        [Test]
        public void ThrowsException_WhenIterationsIsLessThan1()
        {
            var expectedResponse = Guid.NewGuid().ToString();
            var rules = new List<ModulusRule>
            {
                new ModulusRule(new[] {3}, expectedResponse)
            };

            var provider = new ModulusProvider(rules);
            Assert.Throws<FizzBuzzException>(() => { provider.GetResults(-1); });
        }

        [Test]
        public void ThrowsException_WhenRuleMappingsIsNull()
        {
            Assert.Throws<FizzBuzzException>(() =>
            {
                var provider = new ModulusProvider(null);
            });
        }

        [Test]
        public void ThrowsException_WhenRulesAreEmpty()
        {
            Assert.Throws<FizzBuzzException>(() =>
            {
                var provider = new ModulusProvider(new List<ModulusRule>());
            });
        }
    }
}