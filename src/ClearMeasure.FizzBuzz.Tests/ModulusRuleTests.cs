﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ClearMeasure.FizzBuzz.Providers;
using NUnit.Framework;

namespace ClearMeasure.FizzBuzz.Tests
{
    [TestFixture]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ModulusRuleTests
    {
        [Test]
        public void ThrowsException_WhenRuleKeyIs0()
        {
            Assert.Throws<ModulusRuleException>(() =>
            {
                var rules = new List<ModulusRule>
                {
                    new ModulusRule(new[] {0}, "Foo")
                };
            });
        }

        [Test]
        public void ThrowsException_WhenRuleKeyIsEmpty()
        {
            Assert.Throws<ModulusRuleException>(() =>
            {
                var rules = new List<ModulusRule>
                {
                    new ModulusRule(new int[] { }, "Foo")
                };
            });
        }

        [Test]
        public void ThrowsException_WhenRuleOutputIsEmpty()
        {
            Assert.Throws<ModulusRuleException>(() =>
            {
                var rules = new List<ModulusRule>
                {
                    new ModulusRule(new[] {15}, "")
                };
            });
        }

        [Test]
        public void ThrowsException_WhenRuleOutputIsNull()
        {
            Assert.Throws<ModulusRuleException>(() =>
            {
                var rules = new List<ModulusRule>
                {
                    new ModulusRule(new[] {15}, null)
                };
            });
        }
    }
}