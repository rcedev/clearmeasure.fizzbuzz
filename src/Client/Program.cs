﻿using System;
using System.Collections.Generic;
using ClearMeasure.FizzBuzz.Core;
using ClearMeasure.FizzBuzz.Providers;

namespace Client
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var rules = new List<ModulusRule>
            {
                new ModulusRule(new[] {3}, "Fizz"),
                new ModulusRule(new[] {5}, "Buzz"),
                new ModulusRule(new[] {3, 5}, "FizzBuzz")
            };

            IFizzBuzzProvider modulusProvider = new ModulusProvider(rules);
            var fizzBuzzOutput = modulusProvider.GetResults(int.MaxValue);

            foreach (var output in fizzBuzzOutput) Console.WriteLine(output);

            Console.Read();
        }
    }
}